package com.example.homework003_spring_rest_api_handle_exception.repository;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Categories;
import com.example.homework003_spring_rest_api_handle_exception.model.request.AuthorRequest;
import com.example.homework003_spring_rest_api_handle_exception.model.request.CategoryRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface CategoryRepository {
    @Select("SELECT * FROM  categories_tb")
      List<Categories> findAllCategory();

    @Select("SELECT * FROM categories_tb WHERE category_id = #{category_id}")
    Categories getCategoryById(Integer categoryId);

    @Delete("DELETE FROM categories_tb WHERE category_id = #{category_id}")
    boolean deleteCategoryById(@Param("category_id") Integer categoryId);

    @Select("INSERT INTO categories_tb (category_name) VALUES(#{request.category_name} )  " +
            "RETURNING *")
    Categories saveCategory(@Param("request")CategoryRequest categoryRequest);

    @Select("""
            UPDATE categories_tb 
            SET category_name = #{request.category_name}
            WHERE category_id = #{categoryId}
            RETURNING *
            """)
    Categories updateCategory(@Param("request") CategoryRequest categoryRequest, Integer categoryId);


    //--connect(FK) with Book --

    @Select("SELECT c.category_id, c.category_name FROM book_detail bc " +
            "INNER JOIN categories_tb c ON c.category_id = bc.category_id " +
            "WHERE bc.book_id = #{book_id}")
    List<Categories> getCategoryByBookId(Integer bookId);
}
