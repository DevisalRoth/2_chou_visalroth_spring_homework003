package com.example.homework003_spring_rest_api_handle_exception.repository;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.request.AuthorRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepository {

    @Select("SELECT * FROM  author_tb")
    @Result(property = "gender" ,column = "author_gender")
    List<Author>  findAllAuthor();

    @Select("SELECT * FROM author_tb WHERE author_id = #{author_id}")
    @Result(property = "gender" ,column = "author_gender")
    Author getAuthorById(Integer authorId);

    @Delete("DELETE  FROM author_tb WHERE author_id = #{author_id}")
    boolean deleteAuthorById( @Param("author_id") Integer authorId);


    @Select("INSERT INTO author_tb (name, author_gender) VALUES(#{request.name}, #{request.gender})  " +
            "RETURNING author_id")
    Integer saveAuthor(@Param("request") AuthorRequest authorRequest);

    @Select("""
            UPDATE author_tb
            SET name = #{request.name},
            author_gender = #{request.gender}
            WHERE author_id = #{authorId} 
            RETURNING *
            """)
    @Result(property = "gender" ,column = "author_gender")
    Author updateAuthor(@Param("request") AuthorRequest authorRequest, Integer authorId);
}
