package com.example.homework003_spring_rest_api_handle_exception.repository;

import com.example.homework003_spring_rest_api_handle_exception.model.entity.Book;
import com.example.homework003_spring_rest_api_handle_exception.model.request.BooKRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {
    @Select("SELECT * FROM  book_tb")
    @Results(
            id="bookMapper",
            value = {
                    @Result(property = "book_id" , column = "book_id"),
                    @Result(property = "title" ,column = "title"),
                    @Result(property = "publishedDate", column = "published_date"),
                    @Result(property = "author" , column = "author_id",
                            one = @One(select = "com.example.homework003_spring_rest_api_handle_exception.repository.AuthorRepository.getAuthorById")
                    ),

                     @Result(property = "categories",column = "book_id",
                            many  =@Many(select = "com.example.homework003_spring_rest_api_handle_exception.repository.CategoryRepository.getCategoryByBookId")
                    )
            }
    )

    List<Book>  findAllBook();

    @Select("SELECT * FROM  book_tb WHERE book_id = #{bookId}")
    @ResultMap("bookMapper")
    Book getBookById(Integer bookId);



    @Select("INSERT INTO  book_tb(title, published_date, author_id) " +
            "VALUES (#{request.title}, #{request.publishedDate}, #{request.authorId}) " +
            "RETURNING book_id")
    Integer saveBook(@Param("request") BooKRequest booKRequest);

    @Select("INSERT INTO book_detail (book_id ,category_id ) " +
            "VALUES (#{bookId}, #{categoryId})")
    Integer saveCategoryByBookId(Integer bookId, Integer categoryId);



    //delete
    @Delete("delete from book_tb where book_id=#{id}")
    void delete(Integer id);

    @Delete("""
            delete from book_detail where book_id=#{id}
            """)
    void deleteBookDetails(Integer id);


//    update book
    @Select("""
            update book_tb set title=#{book.title} ,author_id=#{book.authorId} where book_id=#{id}
            returning book_id
            """)
    Integer update(Integer id, @Param("book") BooKRequest book);
}
