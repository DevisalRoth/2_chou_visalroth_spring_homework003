package com.example.homework003_spring_rest_api_handle_exception.service;

import com.example.homework003_spring_rest_api_handle_exception.model.entity.Book;
import com.example.homework003_spring_rest_api_handle_exception.model.request.BooKRequest;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();

    Book getBookGetId(Integer bookId);

    Integer addNewBook(BooKRequest booKRequest);

    //delete
    void delete(Integer id);

    //update
    Integer update(Integer id,BooKRequest book);
}
