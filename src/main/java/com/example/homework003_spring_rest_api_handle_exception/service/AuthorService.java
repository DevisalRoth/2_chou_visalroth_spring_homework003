package com.example.homework003_spring_rest_api_handle_exception.service;

import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();
    Author getAuthorById(Integer authorId);

    boolean deleteAuthorById(Integer authorId);
    Integer addNewAuthor(AuthorRequest authorRequest);

    Author updateAuthor(AuthorRequest authorRequest, Integer authorId);
}
