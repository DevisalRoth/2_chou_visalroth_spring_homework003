package com.example.homework003_spring_rest_api_handle_exception.service.serviceImp;
import com.example.homework003_spring_rest_api_handle_exception.exceptionHandler.EmptyException;
import com.example.homework003_spring_rest_api_handle_exception.exceptionHandler.NotFoundException;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Categories;
import com.example.homework003_spring_rest_api_handle_exception.model.request.CategoryRequest;
import com.example.homework003_spring_rest_api_handle_exception.repository.CategoryRepository;
import com.example.homework003_spring_rest_api_handle_exception.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CategoryServiceImp implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Categories> getAllCategory() {

        return categoryRepository.findAllCategory();
    }

    @Override
    public Categories getCategoryById(Integer categoryId) {
        if(categoryRepository.getCategoryById(categoryId)==null){
            throw new EmptyException("categoryId "+categoryId+" not found..");
        }

        return categoryRepository.getCategoryById(categoryId);
    }
    public boolean deleteCategoryById(Integer categoryId){
        if(categoryRepository.getCategoryById(categoryId)==null){
            throw new NotFoundException("categoryId "+categoryId+" not found..");
        }

        return categoryRepository.deleteCategoryById(categoryId);
    }

    @Override
    public Categories addNewCategory(CategoryRequest categoryRequest) {
        if(categoryRequest.getCategory_name().isEmpty()){
            throw new EmptyException("Category name is empty");
        }else if(categoryRequest.getCategory_name().isBlank()){
            throw new EmptyException("Category name is null");
        } else if (Objects.equals(categoryRequest.getCategory_name(),"string")) {
            throw new EmptyException("Category name fill is default String");

        }
        Categories category = categoryRepository.saveCategory(categoryRequest);
        return category;
    }
    public Categories updateCategory(CategoryRequest categoryRequest ,Integer categoryId){
        if(categoryRequest.getCategory_name().isEmpty()){
            throw new EmptyException("Category name is empty");
        }else if(categoryRequest.getCategory_name().isBlank()){
            throw new EmptyException("Category name is null");
        } else if (Objects.equals(categoryRequest.getCategory_name(),"string")) {
            throw new EmptyException("Category name fill is default String");
        }
        return  categoryRepository.updateCategory(categoryRequest,categoryId);
    }


}
