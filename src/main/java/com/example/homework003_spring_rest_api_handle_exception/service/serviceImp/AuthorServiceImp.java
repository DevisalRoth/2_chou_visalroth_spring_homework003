package com.example.homework003_spring_rest_api_handle_exception.service.serviceImp;

import com.example.homework003_spring_rest_api_handle_exception.exceptionHandler.EmptyException;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.request.AuthorRequest;
import com.example.homework003_spring_rest_api_handle_exception.repository.AuthorRepository;
import com.example.homework003_spring_rest_api_handle_exception.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Service
public class AuthorServiceImp implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorRepository.findAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer authorId) {

        if(authorRepository.getAuthorById(authorId)==null){
            throw  new EmptyException("Author Id: "+authorId+" Not Found..!");
        }

        return authorRepository.getAuthorById(authorId);
    }

    @Override
    public boolean deleteAuthorById(Integer authorId) {
        return authorRepository.deleteAuthorById(authorId);
    }

    @Override
    public Integer addNewAuthor(AuthorRequest authorRequest){

        if(authorRequest.getName().isEmpty()||authorRequest.getName().isBlank() &&
                authorRequest.getGender().isEmpty()||authorRequest.getGender().isBlank()){
            throw new EmptyException("Author Name and Gender is null");
        }else if(Objects.equals(authorRequest.getName(), "string")){
            throw new EmptyException("Name cannot default String");
        }else if(Objects.equals(authorRequest.getGender(), "string")){
            throw new EmptyException("Gender cannot default String");
        }
        else {
            Integer authorId = authorRepository.saveAuthor(authorRequest);
            return authorId;
        }




//        Integer authorId = authorRepository.saveAuthor(authorRequest);
//        return authorId;
    }

    @Override
    public Author updateAuthor(AuthorRequest authorRequest, Integer authorId) {
        if( authorRequest.getName().isEmpty() ){
            throw new EmptyException("Author name is empty");
        } else if (authorRequest.getGender().isEmpty()) {
            throw new EmptyException("Gender is empty");
        }
           else if(authorRequest.getName().isBlank()){
               throw  new EmptyException("Dont be blank on author name fill");
        }
           else if (authorRequest.getGender().isBlank()){
               throw new EmptyException("Dont be blank on author gender fill");
        }
        else if(Objects.equals(authorRequest.getName(), "string")){
            throw new EmptyException("Name cannot default String");
        }
        else if(Objects.equals(authorRequest.getGender(), "string")){
            throw new EmptyException("Gender cannot default String");
        }
        return authorRepository.updateAuthor(authorRequest,authorId);
    }
}
