package com.example.homework003_spring_rest_api_handle_exception.service.serviceImp;
import com.example.homework003_spring_rest_api_handle_exception.exceptionHandler.EmptyException;
import com.example.homework003_spring_rest_api_handle_exception.exceptionHandler.NotFoundException;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Book;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Categories;
import com.example.homework003_spring_rest_api_handle_exception.model.request.BooKRequest;
import com.example.homework003_spring_rest_api_handle_exception.repository.BookRepository;
import com.example.homework003_spring_rest_api_handle_exception.repository.CategoryRepository;
import com.example.homework003_spring_rest_api_handle_exception.service.AuthorService;
import com.example.homework003_spring_rest_api_handle_exception.service.BookService;
import com.example.homework003_spring_rest_api_handle_exception.service.CategoryService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private final AuthorService authorService;
    private final BookRepository bookRepository;
    private final CategoryService categoryService;
    private final CategoryRepository categoryRepository;

    public BookServiceImp(AuthorService authorService, BookRepository bookRepository, CategoryService categoryService, CategoryRepository categoryRepository) {
        this.authorService = authorService;
        this.bookRepository = bookRepository;
        this.categoryService = categoryService;
        this.categoryRepository = categoryRepository;
    }
    @Override
    public List<Book> getAllBook() {
        return bookRepository.findAllBook();
    }

    @Override
    public Book getBookGetId(Integer bookId) {
        if(bookRepository.getBookById(bookId)==null){
            throw new NotFoundException("Book Id ="+bookId+" Not found!!");
        }
        return bookRepository.getBookById(bookId);
    }

    @Override
    public Integer addNewBook(BooKRequest booKRequest) {

         authorService.getAuthorById(booKRequest.getAuthorId());
        if (booKRequest.getTitle().isEmpty()) {
            throw new EmptyException("Your title is empty fill !!!");
        }
        Integer storeBookId =bookRepository.saveBook(booKRequest);

        Integer id = bookRepository.saveBook(booKRequest);
        for (int i = 0; i < booKRequest.getCategoryId().size(); i++) {
            categoryService.getCategoryById(booKRequest.getCategoryId().get(i));
            bookRepository.saveCategoryByBookId(id, booKRequest.getCategoryId().get(i));

        }
        return id;

    }

    //delete
    @Override
    public void delete(Integer id) {

        Book book = getBookGetId(id);
        bookRepository.delete(book.getBook_id());
        if(bookRepository.getBookById(id)==null){
            throw new NotFoundException("Book Id ="+id+" Not found!!");
        }
    }

    public Integer update(Integer id, BooKRequest book) {

        authorService.getAuthorById(book.getAuthorId());


        Integer bookId = bookRepository.update(id, book);
        bookRepository.deleteBookDetails(bookId);
        for (int i = 0; i < book.getCategoryId().size(); i++) {
            categoryService.getCategoryById(book.getCategoryId().get(i));
            bookRepository.saveCategoryByBookId(bookId, book.getCategoryId().get(i));
        }

        return bookId;
    }



}
