package com.example.homework003_spring_rest_api_handle_exception.service;

import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Categories;
import com.example.homework003_spring_rest_api_handle_exception.model.request.AuthorRequest;
import com.example.homework003_spring_rest_api_handle_exception.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {
    List<Categories> getAllCategory();
    Categories getCategoryById(Integer categoryId);
    boolean deleteCategoryById(Integer categoryId);
    Categories addNewCategory(CategoryRequest categoryRequest);
    Categories updateCategory(CategoryRequest categoryRequest, Integer categoryId);
}
