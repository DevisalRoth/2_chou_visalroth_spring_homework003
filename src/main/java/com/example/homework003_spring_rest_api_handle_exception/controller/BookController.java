package com.example.homework003_spring_rest_api_handle_exception.controller;

import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Book;
import com.example.homework003_spring_rest_api_handle_exception.model.request.BooKRequest;
import com.example.homework003_spring_rest_api_handle_exception.model.response.AuthorResponse;
import com.example.homework003_spring_rest_api_handle_exception.model.response.BookResponse;
import com.example.homework003_spring_rest_api_handle_exception.repository.BookRepository;
import com.example.homework003_spring_rest_api_handle_exception.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;


@RestController
@RequestMapping("/api/v1/book_tb")
public class BookController {
    private final BookService bookService;


    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all book")
    public ResponseEntity<List<Book>> getAllBook(){

        return ResponseEntity.ok(bookService.getAllBook());
    }

    @GetMapping("/{book_id}")
    @Operation(summary = "Get all book")
    public ResponseEntity<Book> getBookById(@PathVariable("book_id") Integer bookId){
        return ResponseEntity.ok(bookService.getBookGetId(bookId));
    }

    @PostMapping("/save")
    @Operation(summary = "Add new book")
    public ResponseEntity<BookResponse<Book>> addNewBook(@RequestBody BooKRequest booKRequest){
        BookResponse<Book> response =null;
        Integer storeBookId = bookService.addNewBook(booKRequest);
        if(storeBookId != null){
            response = BookResponse.<Book>builder()
                    .message("Success for post book")
                    .payload(bookService.getBookGetId(storeBookId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;

    }

    //delete
    @DeleteMapping(path = "/delete/{book_id}")
    ResponseEntity<?> delete(@PathVariable(value = "book_id") Integer id) {

        BookResponse<AuthorResponse> response = BookResponse.<AuthorResponse>builder()
                .message("Book ID: " + id + " Has Been delete successful")
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .httpStatus(HttpStatus.OK)
//                .payload(null)
                .build();
        bookService.delete(id);
        return ResponseEntity.ok().body(response);
    }



    //update
    @PutMapping("/update/{book_id}")
    public ResponseEntity<?> update(@PathVariable(value = "book_id") Integer id, @RequestBody BooKRequest book) {
        Integer bookId = bookService.update(id, book);
        Book books = bookService.getBookGetId(bookId);

        BookResponse<Book> response = BookResponse.<Book>builder()
                .message("Book Has Been Update successful")
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .httpStatus(HttpStatus.OK)
                .payload(books)
                .build();
        return ResponseEntity.ok().body(response);
    }

}
