package com.example.homework003_spring_rest_api_handle_exception.controller;

import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.request.AuthorRequest;
import com.example.homework003_spring_rest_api_handle_exception.model.response.AuthorResponse;
import com.example.homework003_spring_rest_api_handle_exception.service.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
@RestController
@RequestMapping("/api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all authors")
    public ResponseEntity<AuthorResponse<List<Author>>> getAllAuthor(){

         AuthorResponse<List<Author>> response = AuthorResponse.<List<Author>>builder()
                 .message("Get all author data Successfully")
                 .payload(authorService.getAllAuthor())
                 .httpStatus(HttpStatus.OK)
                 .timestamp(new Timestamp(System.currentTimeMillis()))
                 .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{author_id}")
    @Operation(summary = "Get author by id")
    public ResponseEntity<AuthorResponse<Author>> getAuthorById(@PathVariable("author_id") Integer authorId){

        AuthorResponse<Author> response = null;
        if (authorService.getAuthorById(authorId) != null){
               response=AuthorResponse.<Author>builder()
                    .message("Get author by id successfully")
                    .payload(authorService.getAuthorById(authorId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            response =AuthorResponse.<Author>builder()
                    .message("Data not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/{author_id}")
    @Operation(summary = "Delete author by id")
    public ResponseEntity<AuthorResponse<String>> deleteAuthorById(@PathVariable("author_id")Integer authorId){
        AuthorResponse<String> response = null;
        if(authorService.deleteAuthorById(authorId)) {
            response = AuthorResponse.<String>builder()
                    .message("Delete Successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.notFound().build();
    }
    @PostMapping
    @Operation(summary = "Save new author")
    public ResponseEntity<AuthorResponse<Author>> addNewAuthor(@RequestBody AuthorRequest authorRequest){
        Integer storeAuthorId  = authorService.addNewAuthor(authorRequest);
        if(authorService.addNewAuthor(authorRequest) !=null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .message("Add success")
                    .payload(authorService.getAuthorById(storeAuthorId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{author_id}")
    @Operation(summary = "Update author by id")
    public ResponseEntity<AuthorResponse>
    updateAuthorById(@RequestBody AuthorRequest authorRequest, @PathVariable("author_id") Integer authorId){
        if (authorService.getAuthorById(authorId) !=null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .message("Update author successfully")
                    .payload(authorService.updateAuthor(authorRequest,authorId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            return ResponseEntity.notFound().build();
        }


    }

}
