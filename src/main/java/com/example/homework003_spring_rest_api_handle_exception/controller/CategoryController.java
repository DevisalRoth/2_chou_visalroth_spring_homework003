package com.example.homework003_spring_rest_api_handle_exception.controller;

import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Categories;
import com.example.homework003_spring_rest_api_handle_exception.model.request.AuthorRequest;
import com.example.homework003_spring_rest_api_handle_exception.model.request.CategoryRequest;
import com.example.homework003_spring_rest_api_handle_exception.model.response.AuthorResponse;
import com.example.homework003_spring_rest_api_handle_exception.model.response.CategoryResponse;
import com.example.homework003_spring_rest_api_handle_exception.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {
    private final CategoryService categoryService;
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("/all")
    @Operation(summary = "get all categories")
    public ResponseEntity<CategoryResponse<List<Categories>>> getAllCategories(){
        CategoryResponse<List<Categories>>  response = CategoryResponse.<List<Categories>>builder()
                .message("Get all category data Successfully")
                .payload(categoryService.getAllCategory())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{category_id}")
    @Operation(summary = "Get category by id")
    public ResponseEntity<CategoryResponse<Categories>> getCategoryById(@PathVariable("category_id") Integer categoryId){
       CategoryResponse<Categories> response = null;
       if(categoryService.getCategoryById(categoryId)!= null){
           response = CategoryResponse.<Categories>builder()
                   .message("Get category data by id Successfully")
                   .payload(categoryService.getCategoryById(categoryId))
                   .httpStatus(HttpStatus.OK)
                   .timestamp(new Timestamp(System.currentTimeMillis()))
                   .build();
           return ResponseEntity.ok(response);
       }else{
           response = CategoryResponse.<Categories>builder()
                   .message("data is not found")
                   .httpStatus(HttpStatus.NOT_FOUND)
                   .timestamp(new Timestamp(System.currentTimeMillis()))
                   .build();
           return ResponseEntity.badRequest().body(response);
       }

    }
    @DeleteMapping("/{category_id}")
    @Operation(summary = "Delete category by id")
    public ResponseEntity<CategoryResponse<String>> deleteCategoryById(@PathVariable("category_id") Integer categoryId){
        CategoryResponse<String> response = null;
        if(categoryService.deleteCategoryById(categoryId) ){
            response = CategoryResponse.<String>builder()
                    .message("Delete Successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    @PostMapping
    @Operation(summary = "Save new category")
    public ResponseEntity<CategoryResponse<Categories>> addNewCategory(@RequestBody CategoryRequest categoryRequest){
        Categories storeCategory  = categoryService.addNewCategory(categoryRequest);
        if(categoryService.addNewCategory(categoryRequest) !=null){
            CategoryResponse<Categories> response = CategoryResponse.<Categories>builder()
                    .message("Add success")
                    .payload(storeCategory)
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    @PutMapping("/{category_id}")
    @Operation(summary = "Update category by id")
    public ResponseEntity<CategoryResponse>
    updateCategoryById(@RequestBody CategoryRequest categoryRequest, @PathVariable("category_id") Integer categoryId){
        if (categoryService.getCategoryById(categoryId) !=null){
            CategoryResponse<Categories> response = CategoryResponse.<Categories>builder()
                    .message("Update category successfully")
                    .payload(categoryService.updateCategory(categoryRequest, categoryId ))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            return ResponseEntity.notFound().build();
        }


    }
}
