package com.example.homework003_spring_rest_api_handle_exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeWork003SpringRestApiHandleExceptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeWork003SpringRestApiHandleExceptionApplication.class, args);
    }

}
