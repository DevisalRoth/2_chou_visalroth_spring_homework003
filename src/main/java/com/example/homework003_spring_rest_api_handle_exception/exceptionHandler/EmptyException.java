package com.example.homework003_spring_rest_api_handle_exception.exceptionHandler;

public class EmptyException extends RuntimeException{
    public EmptyException(String message) {
        super(message);
    }
}
