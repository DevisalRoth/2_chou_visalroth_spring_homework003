package com.example.homework003_spring_rest_api_handle_exception.exceptionHandler;

public class NotFoundException extends RuntimeException{

    public NotFoundException(String message) {
        super(message);
    }
}
