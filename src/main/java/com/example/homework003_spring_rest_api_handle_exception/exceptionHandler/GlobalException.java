package com.example.homework003_spring_rest_api_handle_exception.exceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;

@ControllerAdvice
public class GlobalException {
    @ExceptionHandler(EmptyException.class)
    ProblemDetail emptyHandler(EmptyException e){
        ProblemDetail detail =ProblemDetail.forStatusAndDetail(
                HttpStatus.BAD_REQUEST,
                e.getMessage()
        );
        detail.setTitle("Empty fill");
        detail.setType(URI.create("localhost:8080/error/null/data"));
      return detail;
    }

    @ExceptionHandler(NotFoundException.class)
    ProblemDetail notFoundHandler(NotFoundException e){
        ProblemDetail detail = ProblemDetail.forStatusAndDetail(
                HttpStatus.NOT_FOUND,
                e.getMessage()
        );
        detail.setTitle("Data Not Found");
        detail.setType(URI.create("localhost:8080/error/not/found"));

        return detail;
    }

}
