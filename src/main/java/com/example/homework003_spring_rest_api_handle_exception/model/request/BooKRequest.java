package com.example.homework003_spring_rest_api_handle_exception.model.request;


import com.example.homework003_spring_rest_api_handle_exception.model.entity.Author;
import com.example.homework003_spring_rest_api_handle_exception.model.entity.Categories;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.swing.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BooKRequest {
//    private Integer book_id;
    private String title;
    private Timestamp publishedDate;
    private Integer authorId;

    List<Integer>categoryId;
}
