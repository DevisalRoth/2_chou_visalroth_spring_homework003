package com.example.homework003_spring_rest_api_handle_exception.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Author {
    private Integer author_id;
    private String name;
    private String gender;
}
