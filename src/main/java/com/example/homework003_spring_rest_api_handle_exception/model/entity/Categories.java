package com.example.homework003_spring_rest_api_handle_exception.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Categories {
    private Integer category_id;
    private String category_name;
}
