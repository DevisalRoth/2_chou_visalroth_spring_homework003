package com.example.homework003_spring_rest_api_handle_exception.model.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book {
    private Integer book_id;
    private String title;
    private Timestamp publishedDate;
    private Author author;
    List<Categories> categories = new ArrayList<>();

}
