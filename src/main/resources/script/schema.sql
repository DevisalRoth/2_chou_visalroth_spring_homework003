create database book_shop;

create table author_tb(
                          author_id serial primary key ,
                          author_name varchar(50) not null,
                          gender varchar(50)  not null
);

create table book_td(
                        book_id serial primary key ,
                        title varchar(255) not null,
                        published_date timestamp not null,
                        author_id integer not null ,
                        constraint authorId_fk foreign key (author_id) references author_tb
                            on delete cascade on update cascade
);
create table categories_tb(
                              category_id serial primary key,
                              category_name varchar(255) not null
);

create table book_detail(
                            id serial primary key,
                            book_id integer not null,
                            category_id integer not null ,
                            constraint bookId_fk foreign key (book_id) references book_tb
                                on update cascade on delete cascade,
                            constraint categoryId_fk foreign key (category_id) references categories_tb
                                on update cascade on delete cascade
);